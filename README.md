# Authentication API

API to authenticate users.

## Checklist

Follow this checklist to update your project info.

- [ ] Create `.env` file: run `make env`
- [ ] Update `.env` file with authentication info

## Running the app
```bash
# Docker
$ make all

# yarn
$ yarn start
```

## Test

```bash
# unit tests
$ make test

# e2e tests
$ make test-e2e

# test coverage
$ make test-cov
```
