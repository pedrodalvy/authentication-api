import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { mongooseConfig } from './config/mongoose.config';
import { AuthModule } from './modules/auth/auth.module';
import { HealthModule } from './modules/health/health.module';
import { UsersModule } from './modules/users/users.module';

@Module({
  imports: [
    MongooseModule.forRoot(mongooseConfig.uri, mongooseConfig.options),
    HealthModule,
    UsersModule,
    AuthModule,
  ],
})
export class AppModule {}
