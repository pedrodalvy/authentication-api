import 'dotenv/config';

import { MongooseModuleOptions } from '@nestjs/mongoose';
import { env } from 'node:process';

type MongooseConfigType = {
  uri: string;
  options: MongooseModuleOptions;
};

const makeUri = (): string => {
  const user = env.MONGO_USER || 'mongodb';
  const password = env.MONGO_PASSWORD || 'mongodb';
  const host = env.DB_HOST || 'authentication-db';

  return `mongodb://${user}:${password}@${host}/auth`;
};

export const mongooseConfig: MongooseConfigType = {
  uri: makeUri(),
  options: {
    authSource: 'admin',
    serverSelectionTimeoutMS: 2000,
  },
};
