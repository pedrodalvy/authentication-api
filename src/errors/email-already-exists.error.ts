export class EmailAlreadyExistsError extends Error {
  constructor(public readonly email: string) {
    super(`Email already exists: ${email}`);
  }
}
