import { ApiProperty } from '@nestjs/swagger';

export class HealthCheckDTO {
  @ApiProperty({ example: 'Service is up and running.' })
  message: string;
}
