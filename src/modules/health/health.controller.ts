import { Controller, Get } from '@nestjs/common';
import { ApiOkResponse, ApiOperation, ApiTags } from '@nestjs/swagger';

import { HealthCheckDTO } from './dto/health-check.dto';
import { HealthService } from './services/health.service';

@Controller('health')
@ApiTags('Api')
export class HealthController {
  constructor(private readonly healthService: HealthService) {}

  @Get('check')
  @ApiOperation({ summary: 'Health check' })
  @ApiOkResponse({ type: HealthCheckDTO })
  check(): HealthCheckDTO {
    return this.healthService.check();
  }
}
