import * as crypto from 'crypto';
import { mock } from 'jest-mock-extended';
import { Model } from 'mongoose';

import { CreateUserDTO } from '../interfaces/create-user.dto';
import { UserRepository } from '../interfaces/user.repository';
import { userMongoMock } from './mock/user-mongo.mock';
import { UserDocument } from './schemas/user.schema';
import { UserMongoRepository } from './user.mongo.repository';

const mockUserSchema = mock<Model<UserDocument>>({
  create: (userData: CreateUserDTO) => userMongoMock(userData),
  findOne: ({ email }: CreateUserDTO) => ({
    exec: () => userMongoMock({ email }),
  }),
} as unknown as Model<UserDocument>);

describe('UserMongoRepository', () => {
  let repository: UserRepository;

  beforeEach(async () => {
    repository = new UserMongoRepository(mockUserSchema);
  });

  it('should create', async () => {
    // Arrange
    const now = new Date();

    jest.useFakeTimers();
    jest.setSystemTime(now);

    const userData: CreateUserDTO = {
      name: 'John Doe',
      email: 'johndoe@email.com',
      password: 'hashed',
      role: 'user',
    };

    jest.spyOn(crypto, 'randomUUID').mockReturnValueOnce('random-uuid');
    jest.spyOn(mockUserSchema, 'create');

    // Act
    const response = await repository.create(userData);

    // Assert
    expect(mockUserSchema.create).toHaveBeenCalledWith({
      id: 'random-uuid',
      ...userData,
    });

    expect(response.id).toBeDefined();
    expect(response.name).toEqual(userData.name);
    expect(response.email).toEqual(userData.email);
    expect(response.password).toEqual(userData.password);
    expect(response.createdAt).toEqual(now);
  });

  it('should find by email', async () => {
    // Arrange
    const email = 'existing@email.com';
    jest.spyOn(mockUserSchema, 'findOne');

    // Act
    const response = await repository.findByEmail(email);

    // Assert
    expect(mockUserSchema.findOne).toHaveBeenCalledWith({ email });

    expect(response.email).toBe(email);
  });
});
