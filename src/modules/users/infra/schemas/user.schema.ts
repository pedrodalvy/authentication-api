import { Prop, Schema, SchemaFactory } from '@nestjs/mongoose';
import { Document } from 'mongoose';

import { User } from '../../interfaces/user';

@Schema({ timestamps: true })
export class UserMongo implements User {
  @Prop({ type: String, required: true, unique: true })
  readonly id: string;

  @Prop({ type: String, required: true })
  name: string;

  @Prop({ type: String, required: true, unique: true })
  email: string;

  @Prop({ type: String, required: true })
  password: string;

  @Prop({ type: String, required: true })
  role: string;

  @Prop({ type: Date })
  createdAt: Date;

  @Prop({ type: Date })
  updatedAt?: Date;

  @Prop({ type: Date })
  deletedAt?: Date;
}
export const USER_SCHEMA = 'UserSchema';

export type UserDocument = UserMongo & Document;

export const UserSchema = SchemaFactory.createForClass(UserMongo);
