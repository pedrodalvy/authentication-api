import { Injectable } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { randomUUID } from 'crypto';
import { Model } from 'mongoose';

import { CreateUserDTO } from '../interfaces/create-user.dto';
import { User } from '../interfaces/user';
import { UserRepository } from '../interfaces/user.repository';
import { USER_SCHEMA, UserDocument } from './schemas/user.schema';

@Injectable()
export class UserMongoRepository implements UserRepository {
  constructor(
    @InjectModel(USER_SCHEMA) private readonly userModel: Model<UserDocument>,
  ) {}

  async create(partialUser: CreateUserDTO): Promise<User> {
    return this.userModel.create({
      id: randomUUID(),
      ...partialUser,
    });
  }

  async findByEmail(email: string): Promise<User | undefined> {
    return this.userModel.findOne({ email }).exec();
  }
}
