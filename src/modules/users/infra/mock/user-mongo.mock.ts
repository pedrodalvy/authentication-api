import { randomUUID } from 'crypto';

import { User } from '../../interfaces/user';

export const userMongoMock = (userData: Partial<User>): User => {
  return <User>{
    id: randomUUID(),
    createdAt: new Date(),
    ...userData,
  };
};
