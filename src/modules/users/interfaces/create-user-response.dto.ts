export interface CreateUserResponseDTO {
  id: string;
  name: string;
  email: string;
  role: string;
  createdAt: Date;
}
