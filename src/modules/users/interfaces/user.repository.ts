import { CreateUserDTO } from './create-user.dto';
import { User } from './user';

export const USER_REPOSITORY: unique symbol = Symbol();

export interface UserRepository {
  create(user: CreateUserDTO): Promise<User>;
  findByEmail(email: string): Promise<User | undefined>;
}
