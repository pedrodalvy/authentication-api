import { Test, TestingModule } from '@nestjs/testing';
import { mock } from 'jest-mock-extended';

import { EmailAlreadyExistsError } from '../../../errors/email-already-exists.error';
import { CreateUserDTO } from '../interfaces/create-user.dto';
import { User } from '../interfaces/user';
import { USER_REPOSITORY, UserRepository } from '../interfaces/user.repository';
import { CreateUserService } from './create-user.service';

const mockUserRepository = mock<UserRepository>({
  create: (userData: CreateUserDTO) => {
    return Promise.resolve({
      id: 'any-id',
      name: userData.name,
      email: userData.email,
      password: userData.password,
      role: userData.role,
      createdAt: new Date(),
    });
  },
});

describe('CreateUserService', () => {
  let service: CreateUserService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        CreateUserService,
        { provide: USER_REPOSITORY, useValue: mockUserRepository },
      ],
    }).compile();

    service = module.get<CreateUserService>(CreateUserService);
  });

  it('should be create a user', async () => {
    // Arrange
    const userData: CreateUserDTO = {
      name: 'Any Name',
      email: 'any@email.com',
      password: 'any-password',
      role: 'any-role',
    };

    jest.spyOn(mockUserRepository, 'create');

    // Act
    const user = await service.execute(userData);

    // Assert
    expect(mockUserRepository.create).toHaveBeenCalledWith(userData);

    expect(user).toEqual({
      id: user.id,
      name: user.name,
      email: user.email,
      role: user.role,
      createdAt: user.createdAt,
    });
  });

  it('should throw an error if email already exists', async () => {
    // Arrange
    const userData: CreateUserDTO = {
      name: 'Any Name',
      email: 'duplicated@email.com',
      password: 'any-password',
      role: 'any-role',
    };

    jest
      .spyOn(mockUserRepository, 'findByEmail')
      .mockReturnValueOnce({} as Promise<User>);

    try {
      // Act
      await service.execute(userData);
    } catch (error) {
      // Assert
      expect(error).toBeInstanceOf(EmailAlreadyExistsError);
      expect(error.message).toBe(`Email already exists: ${userData.email}`);
    }

    expect.assertions(2);
  });
});
