import { Inject, Injectable } from '@nestjs/common';

import { EmailAlreadyExistsError } from '../../../errors/email-already-exists.error';
import { CreateUserDTO } from '../interfaces/create-user.dto';
import { CreateUserResponseDTO } from '../interfaces/create-user-response.dto';
import { USER_REPOSITORY, UserRepository } from '../interfaces/user.repository';

@Injectable()
export class CreateUserService {
  constructor(
    @Inject(USER_REPOSITORY)
    private readonly repository: UserRepository,
  ) {}

  async execute(userData: CreateUserDTO): Promise<CreateUserResponseDTO> {
    const emailInUse = await this.repository.findByEmail(userData.email);

    if (emailInUse) {
      throw new EmailAlreadyExistsError(userData.email);
    }

    const { id, name, email, role, createdAt } = await this.repository.create(
      userData,
    );

    return { id, name, email, role, createdAt };
  }
}
