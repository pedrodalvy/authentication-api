import { Module } from '@nestjs/common';
import { MongooseModule } from '@nestjs/mongoose';

import { USER_SCHEMA, UserSchema } from './infra/schemas/user.schema';
import { UserMongoRepository } from './infra/user.mongo.repository';
import { USER_REPOSITORY } from './interfaces/user.repository';
import { CreateUserService } from './services/create-user.service';

@Module({
  providers: [
    CreateUserService,
    { provide: USER_REPOSITORY, useClass: UserMongoRepository },
  ],
  imports: [
    MongooseModule.forFeature([{ name: USER_SCHEMA, schema: UserSchema }]),
  ],
  exports: [CreateUserService],
})
export class UsersModule {}
