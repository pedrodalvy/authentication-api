import { Test, TestingModule } from '@nestjs/testing';
import { mock } from 'jest-mock-extended';

import { CreateUserService } from '../../users/services/create-user.service';
import { HashService } from './hash.service';
import { SignUpService } from './sign-up.service';

const mockCreateUserService = mock<CreateUserService>();
const mockHashService = mock<HashService>();

describe('SignUpService', () => {
  let service: SignUpService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [
        SignUpService,
        { provide: CreateUserService, useValue: mockCreateUserService },
        { provide: HashService, useValue: mockHashService },
      ],
    }).compile();

    service = module.get(SignUpService);
  });

  it('should be sign up a user', async () => {
    // Arrange
    const signUpData = {
      name: 'John Doe',
      email: 'johndoe@email.com',
      password: '123456',
    };

    jest
      .spyOn(mockHashService, 'make')
      .mockReturnValueOnce(Promise.resolve('hashed-password'));

    jest.spyOn(mockCreateUserService, 'execute').mockReturnValueOnce(
      Promise.resolve({
        id: 'any-id',
        name: signUpData.name,
        email: signUpData.email,
        password: 'hashed-password',
        role: 'user',
        createdAt: new Date('2020-01-01'),
      }),
    );

    // Act
    const user = await service.execute(signUpData);

    // Assert
    expect(mockCreateUserService.execute).toHaveBeenCalledWith({
      ...signUpData,
      password: 'hashed-password',
      role: 'user',
    });

    expect(user).toEqual({
      id: 'any-id',
      name: signUpData.name,
      email: signUpData.email,
    });
  });

  it('should throw if CreateUserService throws', async () => {
    // Arrange
    const signUpData = {
      name: 'John Doe',
      email: 'johndoe@email.com',
      password: '123456',
    };

    const someError = new Error('some error');
    jest.spyOn(mockCreateUserService, 'execute').mockImplementationOnce(() => {
      throw someError;
    });

    // Act & Assert
    await expect(service.execute(signUpData)).rejects.toThrow(someError);
  });
});
