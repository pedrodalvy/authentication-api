import { Test, TestingModule } from '@nestjs/testing';

import { HashService } from './hash.service';

jest.mock('bcrypt', () => ({
  hash: jest.fn(() => 'hashed-password'),
}));

describe('HashService', () => {
  let service: HashService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [HashService],
    }).compile();

    service = module.get<HashService>(HashService);
  });

  it('should encrypt a string', async () => {
    // Arrange
    const plainText = 'any_plain_text';

    // Act
    const hash = await service.make(plainText);

    // Assert
    expect(hash).toBeDefined();
    expect(hash).not.toBe(plainText);
  });
});
