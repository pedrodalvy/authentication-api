import { Injectable } from '@nestjs/common';

import { CreateUserService } from '../../users/services/create-user.service';
import { SignUpDTO } from '../interfaces/sign-up.dto';
import { SignUpResponseDTO } from '../interfaces/sign-up-response.dto';
import { HashService } from './hash.service';

@Injectable()
export class SignUpService {
  constructor(
    private readonly createUserService: CreateUserService,
    private readonly hashService: HashService,
  ) {}

  async execute(signUpData: SignUpDTO): Promise<SignUpResponseDTO> {
    const hashedPassword = await this.hashService.make(signUpData.password);

    const { id, name, email } = await this.createUserService.execute({
      ...signUpData,
      password: hashedPassword,
      role: 'user',
    });

    return { id, name, email };
  }
}
