import { Injectable } from '@nestjs/common';
import { hash } from 'bcrypt';

import { appConfig } from '../../../config/app.config';

@Injectable()
export class HashService {
  async make(plainText: string): Promise<string> {
    return hash(plainText, appConfig.auth.hashSaltRounds);
  }
}
