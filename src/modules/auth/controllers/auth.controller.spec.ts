import { Test, TestingModule } from '@nestjs/testing';
import { mock } from 'jest-mock-extended';

import { SignUpDTO } from '../interfaces/sign-up.dto';
import { SignUpService } from '../services/sign-up.service';
import { AuthController } from './auth.controller';

const mockSignUpService = mock<SignUpService>({
  execute: (signUpData: SignUpDTO) => {
    return Promise.resolve({
      id: 'any-id',
      name: signUpData.name,
      email: signUpData.email,
    });
  },
});

describe('AuthController', () => {
  let controller: AuthController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [AuthController],
      providers: [{ provide: SignUpService, useValue: mockSignUpService }],
    }).compile();

    controller = module.get<AuthController>(AuthController);
  });

  it('should sign up a user', async () => {
    // Arrange
    const signUpData: SignUpDTO = {
      name: 'John Doe',
      email: 'johndoe@email.com',
      password: '123456',
    };

    jest.spyOn(mockSignUpService, 'execute');

    // Act
    const response = await controller.signUp(signUpData);

    // Assert
    expect(mockSignUpService.execute).toHaveBeenCalledWith(signUpData);

    expect(response).toEqual({
      message: 'User created successfully',
    });
  });

  it('should throw if SignUpService throws', async () => {
    // Arrange
    const signUpData: SignUpDTO = {
      name: 'John Doe',
      email: 'johndoe@email.com',
      password: '123456',
    };

    const someError = new Error('some error');

    jest.spyOn(mockSignUpService, 'execute').mockImplementationOnce(() => {
      throw someError;
    });

    // Act & Assert
    await expect(controller.signUp(signUpData)).rejects.toThrow(someError);
  });
});
