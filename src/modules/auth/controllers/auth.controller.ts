import { Body, Controller, Post } from '@nestjs/common';

import { SignUpDTO } from '../interfaces/sign-up.dto';
import { SignUpControllerResponseDto } from '../interfaces/sign-up-controller-response.dto';
import { SignUpService } from '../services/sign-up.service';

@Controller('auth')
export class AuthController {
  constructor(private readonly signUpService: SignUpService) {}

  @Post('sign-up')
  async signUp(
    @Body() signupDTO: SignUpDTO,
  ): Promise<SignUpControllerResponseDto> {
    await this.signUpService.execute(signupDTO);

    return { message: 'User created successfully' };
  }
}
