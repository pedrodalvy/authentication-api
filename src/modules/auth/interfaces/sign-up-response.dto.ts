export interface SignUpResponseDTO {
  id: string;
  name: string;
  email: string;
}
