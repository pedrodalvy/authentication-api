import { Module } from '@nestjs/common';

import { UsersModule } from '../users/users.module';
import { AuthController } from './controllers/auth.controller';
import { HashService } from './services/hash.service';
import { SignUpService } from './services/sign-up.service';

@Module({
  controllers: [AuthController],
  providers: [SignUpService, HashService],
  imports: [UsersModule],
})
export class AuthModule {}
